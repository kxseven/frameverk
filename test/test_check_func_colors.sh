#!/usr/bin/env bash

# The PROJECT_ROOT is one level up from where the scripts run in the 'bin'
# directory
PROJECT_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )

# Load the Frameverk
. ${PROJECT_ROOT}/etc/frameverkrc

echo -e "${__fg_grey}GREY${__no_color}"
echo -e "${__fg_white}WHITE${__no_color}"
echo -e "${__fg_cyan}CYAN${__no_color}"
echo -e "${__fg_red}RED${__no_color}"
echo -e "${__fg_green}GREEN${__no_color}"
echo -e "${__fg_yellow}YELLOW${__no_color}"
