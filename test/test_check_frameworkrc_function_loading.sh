#!/usr/bin/env bash

# The PROJECT_ROOT is one level up from where the scripts run in the 'bin'
# directory
PROJECT_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )

# Load the Frameverk
. ${PROJECT_ROOT}/etc/frameverkrc

# Test to ensure functions are loaded
all_func_names=$(grep -oiEh '^_.*\(\)' ${PROJECT_ROOT}/etc/functions.d/* | sed -e 's/()//g')

for next_func_name in $all_func_names
do
    echo "function $next_func_name found"
    declare -f $next_func_name > /dev/null
    [ $? -eq 0 ] && echo "function $next_func_name loaded"
done
