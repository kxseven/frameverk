#!/usr/bin/env bash

# The PROJECT_ROOT is one level up from where the scripts run in the 'bin'
# directory
PROJECT_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )

# Load the Frameverk
. ${PROJECT_ROOT}/etc/frameverkrc

# Test for auto-created dirs
[ -d $PROJECT_ROOT/log ] && echo log_dir_OK
[ -d $PROJECT_ROOT/tmp ] && echo tmp_dir_OK
