#!/usr/bin/env bash

# The PROJECT_ROOT is one level up from where the scripts run in the 'bin'
# directory
PROJECT_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )

CONST_COMMAND_LINE="$@"
CONST_OS_VERSION=$(uname -r)
CONST_SYSTEM_TYPE=$(uname -m)
CONST_SCRIPT_NAME=${0##*/}

# Load the Frameverk
. ${PROJECT_ROOT}/etc/frameverkrc

# Current logging config
TTY_OUTPUT_ENABLED=$TRUE
LOG_OUTPUT_ENABLED=$TRUE
SYSLOG_OUTPUT_ENABLED=$FALSE
EVENT_OUTPUT_ENABLED=$TRUE

# Default log file
DEFAULT_OUT="${PROJECT_ROOT}/log/${CONST_SCRIPT_NAME}.log"
EVENT_LOG="${PROJECT_ROOT}/log/${CONST_SCRIPT_NAME}_events.log"
TTY_LOG="${PROJECT_ROOT}/log/${CONST_SCRIPT_NAME}_tty.log"
export DEFAULT_OUT EVENT_LOG TTY_LOG

[ -f $DEFAULT_OUT ] && rm -f $EVENT_LOG
[ -f $EVENT_LOG ] && rm -f $DEFAULT_OUT

message_text="Example test message"

exec 2> $TTY_LOG

_log "$LINENO" "$message_text"
grep -q "034 INFO  ${message_text}" $TTY_LOG
echo $?
grep -q ": test_check_func_log.sh:0034 : ${message_text}" $DEFAULT_OUT
echo $?

_log_event "$LINENO" "WARNING" "$message_text"
grep -q ",040 : WARNING : ${message_text}" $EVENT_LOG
echo $?
_log_event "$LINENO" "ERROR" "$message_text"
grep -q ",043 : ERROR   : ${message_text}" $EVENT_LOG
echo $?
_log_event "$LINENO" "INFO" "$message_text"
grep -q ",046 : INFO    : ${message_text}" $EVENT_LOG
echo $?


[ -f $DEFAULT_OUT ] && rm -f $EVENT_LOG
[ -f $EVENT_LOG ] && rm -f $DEFAULT_OUT
[ -f $TTY_LOG ] && rm -f $TTY_LOG
